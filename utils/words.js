(function () {
  'use strict'

  const writeBit = (buf, pos, val) => {
    let byteIndex = Math.floor(pos / 8)
    let remainder = pos % 8
    let byteVal = buf.readUInt8(byteIndex)
    // modifica bit no byte e escreve no db
    let newByte = (val) ? byteVal | (1 << remainder) : byteVal & ~(1 << remainder)

    return buf.writeUInt8(newByte, byteIndex)
  }

  const readBit = (buf, pos) => {
    let byteIndex = Math.floor(pos / 8)
    let remainder = pos % 8
    let byteVal = buf.readUInt8(byteIndex)
    return (byteVal & (1 << remainder)) ? 0x01 : 0x00
  }

  const readByte = (buf, pos) => buf.readUInt8(pos)
  const writeByte = (buf, pos, val) => buf.writeUInt8(val, pos)
  const readWord = (buf, pos) => buf.readUInt16BE(pos)
  const writeWord = (buf, pos, val) => buf.writeUInt16BE(val, pos)
  const readDWord = (buf, pos) => buf.readUInt32BE(pos)
  const writeDWord = (buf, pos, val) => buf.writeUInt32BE(val, pos)
  const readReal = (buf, pos) => buf.readFloatBE(pos)
  const writeReal = (buf, pos, val) => buf.writeFloatBE(val, pos)
  const readSByte = (buf, pos) => buf.readInt8(pos)
  const writeSByte = (buf, pos, val) => buf.writeInt8(val, pos)
  const readSWord = (buf, pos) => buf.readInt16BE(pos)
  const writeSWord = (buf, pos, val) => buf.writeInt16BE(val, pos)
  const readSDWord = (buf, pos) => buf.readInt32BE(pos)
  const writeSDWord = (buf, pos, val) => buf.writeInt32BE(val, pos)
  const readString = (buf, pos) => buf.toString('utf8', pos, (pos + words.string.size)).replace(/\0/g, '')
  const writeString = (buf, pos, val) => {
    let string = Buffer.from(val, 'utf8')
    let stringTamFixo = Buffer.alloc(words.string.size, 0x00)
    string.copy(stringTamFixo)
    stringTamFixo.copy(buf, pos)
    return buf   
  }

  const words = {
    bit:    { word: 0x01, size: 1, readFunc: readBit, writeFunc: writeBit },
    byte:   { word: 0x02, size: 1, readFunc: readByte, writeFunc: writeByte },
    word:   { word: 0x04, size: 2, readFunc: readWord, writeFunc: writeWord },
    dword:  { word: 0x06, size: 4, readFunc: readDWord, writeFunc: writeDWord },
    real:   { word: 0x08, size: 4, readFunc: readReal, writeFunc: writeReal },
    string: { word: 0x02, size: 26, readFunc: readString, writeFunc: writeString }, // tamanho padrão TCS
    // signeds
    sbyte:   { word: 0x02, size: 1, readFunc: readSByte, writeFunc: writeSByte },
    sword:   { word: 0x04, size: 2, readFunc: readSWord, writeFunc: writeSWord },
    sdword:  { word: 0x06, size: 4, readFunc: readSDWord, writeFunc: writeSDWord }
  }
  
  module.exports = words
})()