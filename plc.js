(function() {

  const snap7 = require('node-snap7')
  const s7server = new snap7.S7Server()
  const words = require('./utils/words')

  let dbs = []
  let networks = []

  // Configura envent listener
  // s7server.on('event', function(event) {
  //   console.log('Event:', s7server.EventText(event))
  // })

// *****************************************************************
// RUNNER   
// *****************************************************************

  // Inicía servidor em 0.0.0.0  
  const start = () => {
    return new Promise((res, rej) => {
      s7server.Start((err) => {
        (err) ? rej(s7server.ErrorText(err)) : res()
      })
    })
  }

  // Pára servidor  
  const stop = () => {
    s7server.Stop()
  }

  // inicia processamento. Realiza scans a cada <intervalo> ms  
  const run = (interval) => {
    setInterval(scan, interval)
  }

  // roda todas as networks inseridas  
  const scan = () => {
    networks.forEach((net, index) => {
      // verifica condições
      // TODO: atualmente, condições são apenas da forma AND, criar método para verificar lógicas mais complexas de acordo com a necessidade
      if (!net.condicoes) console.log('Erro: condição na network inválida')
      else{
        let promises = net.condicoes.map((cond) => 
          getVarPLC(cond.variavel)
            .then((valorLido) => valorLido === cond.valor)
        )
        Promise.all(promises)
          .then((results) => {
            return results.reduce((check, cur) => check && cur, true)
          })
          .then((pass) => {
            //console.log('[Network]', net.nome, '[Resultado condições]', pass)
            // se condiões atendidas, realiza ações
            // TODO: atualmente, ações são apenas escrita direta em variáveis. Criar método com delay
            if (pass) {
              net.acoes.forEach((act) => {
                // console.log('[Network]', net.nome, '[Ação]', JSON.stringify(act))
                setVarPLC(act.variavel, act.valor)
              })
            }
          })
          .catch((err) => console.log(`[ERRO] Execução da network ${net.nome}: ${err}`))
      }
    })
  }

// *****************************************************************
// DB's   
// *****************************************************************

  const getDBByIndex = (index) => {
    return new Promise((res, rej) => {
      let db = null
      try {
        db = s7server.GetArea(s7server.srvAreaDB, index)  
      } 
      catch (e) {}
      finally {
        res(db)
      }
    })
  }
  
  const setDBByIndex = (index, db) => {
    return new Promise((res, rej) => {
      try {
        s7server.SetArea(s7server.srvAreaDB, index, db)
        res()
      } 
      catch (e) {
        rej(e)
      }
    })
  }  

  // inscreve DB no servidor
  const registerDB = (index, buf) => {
    return getDBByIndex(index)
      .then((db) => {
        if (!db) {
          if (s7server.RegisterArea(s7server.srvAreaDB, index, buf)) {
            console.log(`DB '${index}' inscrito no PLC`)
            dbs.push(index)
          } 
          else throw new Error(`Erro ao inscrever DB '${index}`)
        } 
        else throw new Error(`DB '${index}' já existe`)
      })
  }

  // desinscreve DB do servidor
  const unregisterDB = (index) => {
    return getDBByIndex(index)
      .then((db) => {
        if (db) {
          if (s7server.UnregisterArea(s7server.srvAreaDB, index)) {
            dbs = dbs.filter(function(i) {
              return i !== index
            })
            console.log(`DB '${index}' desinscrito do PLC`)
            } 
            else throw new Error(`Erro ao desinscrever DB '${index}'`)
          } else throw new Error(`DB '${index}' não existe`)
        })
  }
  
  // desinscreve todos DBs do servidor
  const unregisterAllDBs = () => {
    return new Promise((res, rej) => {
      dbs.forEach((index) => { s7server.UnregisterArea(s7server.srvAreaDB, index) })
      dbs = []
      res()
    })
  }

  const fillDB = (index, val) => {
    return getDBByIndex(index)
      .then((db) => {
        if (db) {
          db.fill(val)
          s7server.SetArea(s7server.srvAreaDB, index, db)   
        } 
        else throw new Error(`DB '${index}' não existe`)
      })
  }

  const loadDB = (index, vals) => {
    return getDBByIndex(index)
      .then((db) => {
        if (db) {
          vals.forEach((val) => {
            words[val.palavra].writeFunc(db, val.posicao * words[val.palavra].size, val.valor)
          })
          s7server.SetArea(s7server.srvAreaDB, index, db)
        } 
        else throw new Error(`DB '${index}' não existe`)
      })
  }

  const getDB = (index) => {
    return getDBByIndex(index)
      .then((db) => {
        if (db) {
          return db
        } 
        else throw new Error(`DB '${index}' não existe`)
      })
  }

// *****************************************************************
// NETWORK's   
// *****************************************************************

  const insertNetwork = (net) => {
    return checkNetwork(net)
      .then(() => {
        networks.push(net)
        console.log(`network '${net.nome}' inserida do PLC.`)
      })
  }

  const removeNetwork = (nome) => {
    return new Promise((res, rej) => {
      let removida = false
      networks = networks.filter((nw) => {
        if (nw.nome !== nome) return true
        else {
          removida = true
          console.log(`network '${nome}' removida do PLC.`)
          return false
        }
      })
      if (removida) res()
      else rej(`Network '${nome}' não existe`)
    })
  }

  const removeAllNetworks = () => {
    networks = []
    console.log('Todas as networks removidas do PLC.')
    return Promise.resolve()
  }

  const checkNetwork = (net) => {
    return new Promise((res, rej) => {
      // verifica se existe
      let existe = networks.some((n) => net.nome === n.nome)
      if (existe) {
        rej(`network "${net.nome}" já existe`)
      }
      res()
    })
  }

// *****************************************************************
// READ/WRITE   
// *****************************************************************

  const getVarPLC = (v) => {
    switch (v.area) {
      case 'DB': 
        return getVarDB(v.areaIndex, v.palavra, v.posicao)
      default: 
        throw new Error(`Erro: area '${v.area}' não existe`)
    }
  }
  
  const getVarDB = (dbIndex, wordType, pos) => {
    return getDBByIndex(dbIndex)
      .then((db) => {      
        if (db) {
          if (words[wordType])
            return words[wordType].readFunc(db, pos)
          else 
            throw new Error(`Erro: palavra '${wordType}' não existe`)
        } 
        else 
          throw new Error(`Erro: DB '${dbIndex}' não existe`)
      })
  }

  const setVarPLC = (v, val) => {
    switch (v.area) {
      case 'DB': 
        return setVarDB(v.areaIndex, v.palavra, v.posicao, val)
      default: 
        throw new Error(`Erro: area '${v.area}' não existe`)
    }
  }

  const setVarDB = (dbIndex, wordType, pos, val) => {
    return getDBByIndex(dbIndex)
      .then((db) => {      
        if (db) {
          if (words[wordType]){
            value = words[wordType].writeFunc(db, pos, val)
            return setDBByIndex(dbIndex, db)
              .then(() => value)
          }
          else 
            throw new Error(`Erro: palavra '${wordType}' não existe`)
        } 
        else
          throw new Error(`Erro: DB '${dbIndex}' não existe`)
      })
  }

  module.exports = {
    start: start,
    stop: stop,
    run: run,
    getDB: getDB,
    registerDB: registerDB,
    unregisterDB: unregisterDB,
    unregisterAllDBs: unregisterAllDBs,
    fillDB: fillDB,
    loadDB: loadDB,
    getVarPLC: getVarPLC,
    getVarDB: getVarDB,
    setVarPLC: setVarPLC,
    setVarDB: setVarDB,
    insertNetwork: insertNetwork,
    removeNetwork: removeNetwork,
    removeAllNetworks: removeAllNetworks
  }

})()