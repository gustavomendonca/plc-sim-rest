(function () {
  const express = require('express')
  const app = express()
  const bodyParser = require('body-parser')
  const plcRoutes = require('./routes/plc.route')
  const plc = require('./plc')

  const apiPort = process.env.PLC_API_PORT || 8866

  app.use(bodyParser.json())

  app.use((req, res, next) => {
    let now = new Date
    console.log(`[${now.toISOString().replace('T', ' ').slice(0, 23)}] ${req.method} em ${req.path}`)
    next()
  })

  app.use('/plc', plcRoutes)

  // inicia API de controle  
  app.listen(apiPort, function () {
    console.log('API do PLC server iniciado na porta', apiPort)
  })

  // inicia plc  
  plc
    .start()
    .then(() => {
      console.log('Simulador de plc inicializado')
      plc.run(100) // scan cycle de 100ms
    })
    .catch((err) => {
      console.log('[ERRO]', err)
  })

})()