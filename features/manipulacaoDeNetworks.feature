#encoding: utf-8
#language: pt

Funcionalidade: Manipulação de Networks no simulador de PLC via API REST
    Como um usuário
    Eu quero manipular as networks do simulador de PLC
    Para produzir o ambiente de teste que necessito

Cenário de Fundo:
    Dado que exista um DB com índice 100 no simulador de PLC

Cenário: Funcionamento de Network no simulador de PLC
    Dado que eu insira no simulador uma Network com as características:
        | nome    | condicoes                                                                           | acoes                                                                             |
        | lifebit | [{"variavel":{"area":"DB","areaIndex":100,"palavra":"bit","posicao": 2},"valor":1}] | [{"variavel":{"area":"DB","areaIndex":100,"palavra":"bit","posicao":2},"valor": 0}] |
    Então o valor da variável "bit" na posição 2 do DB 100 será "0"
    Quando eu ativar o bit 2 do DB 100
    E aguardar 100 ms
    Então o valor da variável "bit" na posição 2 do DB 100 será "0"
    Dado que eu remova do simulador a Network "lifebit"
    Quando eu ativar o bit 2 do DB 100
    E aguardar 100 ms
    Então o valor da variável "bit" na posição 2 do DB 100 será "1"

Cenário: Funcionamento de Network no simulador de PLC (2)
    Dado que eu insira no simulador uma Network com as características:
        | nome    | condicoes                                                                               | acoes                                                                             |
        | net2    | [{"variavel":{"area":"DB","areaIndex":100,"palavra":"byte","posicao": 10},"valor":123}] | [{"variavel":{"area":"DB","areaIndex":100,"palavra":"word","posicao":55},"valor": 5554}] |
    Então o valor da variável "word" na posição 55 do DB 100 será "0"
    Quando eu definir o valor "47" para a variável "byte" na posição 10 do DB 100
    E aguardar 100 ms
    Então o valor da variável "word" na posição 55 do DB 100 será "0"
    Quando eu definir o valor "123" para a variável "byte" na posição 10 do DB 100
    E aguardar 100 ms
    Então o valor da variável "word" na posição 55 do DB 100 será "5554"
    Dado que eu remova do simulador a Network "net2"
    E que haja uma variável "word" na posição 55 do DB 100 com valor "0"
    Quando eu definir o valor "123" para a variável "byte" na posição 10 do DB 100
    E aguardar 100 ms
    Então o valor da variável "word" na posição 55 do DB 100 será "0"