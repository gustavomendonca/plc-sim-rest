(function () {

  const utils = require('../support/utils')
  const words = require('../../utils/words')

  const {
    defineSupportCode
  } = require('cucumber')

  defineSupportCode(function ({
    Given,
    When,
    Then
  }) {

    // utilizado para ajudar no debug. Inserir "Então pausa para debug" na história e colocar breakpoint nesta função no devtools
    Then('pausa para debug', function (cb) {
      cb()
    })

    Given('que eu insira no simulador um DB com as características:', function (table, cb) {
      let index = parseInt(table.rawTable[1][0], 10)
      let payload = {
        size: parseInt(table.rawTable[1][1], 10),
        fillValue: parseInt(table.rawTable[1][2], 10)
      }
      utils
        .doPost('plc', 'db' + '/' + index, 'register', payload)
        .then(() => cb())
        .catch((err) => cb(err))
    })
    
    Then('o DB de índice {int} existe', function (index, cb) {
      this.s7client.GetAgBlockInfo(this.s7client.Block_DB, index, (err, result) => {
        if (err) cb(this.s7client.ErrorText(err))
        cb((result) ? null : 'DB ' + index + ' não existe')
      })
    })

    Then('o DB de índice {int} não existe', function (index, cb) {
      this.s7client.GetAgBlockInfo(this.s7client.Block_DB, index, (err, result) => {
        if (err) cb()
        cb((result) ? 'DB ' + index + ' existe' : null)
      })
    })

    Then('o DB de índice {int} possui tamanho {int} bytes', function (index, size, cb) {
      this.s7client.GetAgBlockInfo(this.s7client.Block_DB, index, (err, db) => {
        if (err) cb(this.s7client.ErrorText(err))
        cb((db.MC7Size === size) ? null : 'DB ' + index + ' possui ' + db.MC7Size + '. Esperado tamanho ' + size)
      })
    })

    Then('o DB de índice {int} possui todas as posições com valor {int}', function (index, val, cb) {
      this.s7client.GetAgBlockInfo(this.s7client.Block_DB, index, (err, db) => {
        this.s7client.DBRead(index, 0, db.MC7Size, (err, result) => {
          if (err) cb(this.s7client.ErrorText(err))
          else {
            for (let v of result.values()){
              if (v !== val) cb('Valores iniciais não correspondem')
            }
            cb()
          }
        })
      })
    })

    Given('que exista um DB com índice {int} no simulador de PLC', function (index, cb) {
      let payload = {
        size: 1280,
        fillValue: 0
      }
      utils
        .doPost('plc', 'db' + '/' + index, 'register', payload)
        .then(() => cb())
        .catch((err) => cb(err))
    })

     Given('que não exista um DB com índice {int} no simulador de PLC', function (index, cb) {
      utils
        .doDelete('plc', 'db' + '/' + index, 'unregister', null)
        .then(() => cb())
        .catch((err) => {
          cb(err)
        })
    })

    When('eu remover o DB de índice {int} do simulador de PLC', function (index, cb) {
      utils
        .doDelete('plc', 'db' + '/' + index, 'unregister')
        .then((res) => {
          this.lastHttpValResponse = res.body
          cb()
        })
        .catch((err) => {
          this.lastHttpValResponse = res.body
          cb(err)
        })
    })

    When('eu remover todos os DB\'s do simulador de PLC', function (cb) {
      utils
        .doDelete('plc', 'db', 'unregisterAll')
        .then(() => cb())
        .catch((err) => cb(err))
    })

    Given('que os seguintes valores estejam no DB de índice {int}:', function (index, table, cb) {
      let self = this
      let len = table.rows().reduce((last, cur) => {
        let pos = parseInt(cur[1], 10)
        let lastByte = pos + words[cur[0]].size
        return (lastByte > last) ? lastByte : last
      }, 0)
      let buf = Buffer.alloc(len)
      table.rows().forEach((val) => {
        let pos = parseInt(val[1])
        words[val[0]].writeFunc(buf, val[1], val[2])
      })
      self.s7client.DBWrite(index, 0, buf.length, buf, (err) => { cb(err ? elf.s7client.ErrorText(err) : null) })
    })

    Then('os seguintes valores estão no DB de índice {int}:', function (index, table, cb) {
      let self = this
      table.rows().forEach((val) => {
        let pos = parseInt(val[1])
        self.s7client.DBRead(index, pos, words[val[0]].size, (err, buf) => {
          if (err) cb(self.s7client.ErrorText(err))
          else {
            let valueOnPlc = words[val[0]].readFunc(buf, 0)
            cb((valueOnPlc == val[2]) ? null : val[0] + ' diferente encontrado na posição ' + val[1] + '. Esperado ' + val[2] + ', encontrado ' + valueOnPlc)
          }
        })
      })
    })

    When('eu preencher o DB de índice {int} com o valor {int}', function (index, val, cb) {
      let self = this
      self.s7client.GetAgBlockInfo(this.s7client.Block_DB, index, (err, result) => {
        if (err) cb(this.s7client.ErrorText(err))
        else {
          let size = result.MC7Size
          let buf = Buffer.alloc(size, val, 'utf8')
          self.s7client.DBWrite(index, 0, size, buf, (err) => {
            cb((err) ? self.s7client.ErrorText(err) : null)
          })
        }
      })
    })

    When('eu limpar o DB de índice {int}', function (index, cb) {
      utils
        .doPost('plc', 'db' + '/' + index, 'clear')
        .then(() => cb())
        .catch((err) => cb(err))
    })

    When('eu carregar o DB com índice {int} com os valores:', function (index, table, cb) {
      let payload = {
        valores: table.rows().map((row) => {
          return {
            palavra: row[0],
            posicao: parseInt(row[1], 10),
            valor: row[2]  
          }
        })
      }
      utils
        .doPost('plc', 'db' + '/' + index, 'load', payload)
        .then(() => cb())
        .catch((err) => cb(err))
    })

    Then('recebo resposta {stringInDoubleQuotes}', function (res, cb) {
      let lastHttpResponse = this.lastHttpValResponse
      cb((lastHttpResponse.indexOf(res) > -1)? null : 'Resposta esperada divergente. Esperado "' + res + '", recebido ' + lastHttpResponse);
    })

  })

})()