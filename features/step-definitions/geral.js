(function () {

  const {
    defineSupportCode
  } = require('cucumber')

  defineSupportCode(function ({
    Given,
    When,
    Then
  }) {

    When('aguardar {int} ms', {timeout: 600000},function (ms, cb) {
      setTimeout(cb, ms)
    })

  })
})()