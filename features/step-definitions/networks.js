(function () {

  const utils = require('../support/utils')
  
  const {
    defineSupportCode
  } = require('cucumber')

  defineSupportCode(function ({
    Given,
    When,
    Then
  }) {

    Given('que eu insira no simulador uma Network com as características:', function (table, cb) {
      let nome = table.rawTable[1][0]
      let condicoes = JSON.parse(table.rawTable[1][1])
      let acoes = JSON.parse(table.rawTable[1][2])

      let payload = {
        nome: nome,
        condicoes: condicoes,
        acoes: acoes
      }
      utils
        .doPost('plc', 'network', 'insert', payload)
        .then(() => cb())
        .catch((err) => cb(err))
    })

    Given('que eu remova do simulador a Network {stringInDoubleQuotes}', function (network, cb) {
      let payload = {
        nome: network
      }
      utils
        .doDelete('plc', 'network', 'remove', payload)
        .then(() => cb())
        .catch((err) => cb(err))
    })

  })
})()