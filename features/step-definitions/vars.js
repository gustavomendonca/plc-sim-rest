(function () {

  const utils = require('../support/utils')
  const words = require('../../utils/words')

  const {
    defineSupportCode
  } = require('cucumber')

  const parseValor = (val, palavra) => {
    let valor = null
    switch(palavra){
      case 'real': valor = parseFloat(val)
      break
      case 'string': valor = val
      break
      default: valor = parseInt(val, 10)
    }
    return valor
  }

  defineSupportCode(function ({
    Given,
    When,
    Then
  }) {
    
    When('eu ativar o bit {int} do DB {int}', function (bitIndex, dbIndex, cb) {
      utils
        .doPost('plc', 'db' + '/' + dbIndex + '/bit/' + bitIndex, 'set')
        .then(() => cb())
        .catch((err) => cb(err))
    })
    
    When('eu desativar o bit {int} do DB {int}', function (bitIndex, dbIndex, cb) {
      utils
        .doPost('plc', 'db' + '/' + dbIndex + '/bit/' + bitIndex, 'reset')
        .then(() => cb())
        .catch((err) => cb(err))
    })

    Then('o valor da variável {stringInDoubleQuotes} na posição {int} do DB {int} será {stringInDoubleQuotes}', function (wordType, pos, dbIndex, val, cb) {
      let esperado = parseValor(val, wordType)
      let qtdALer = (wordType == 'string') ? 26 : 1

      this.s7client.ReadArea(this.s7client.S7AreaDB, dbIndex, pos, qtdALer, words[wordType].word, (err, v) => {
        if (err) cb(this.s7client.ErrorText(err))
        else {
          let recebido = words[wordType].readFunc(v, 0)
          switch (wordType){
            case 'real': 
              cb((Math.abs(recebido - esperado) < 0.0001) ? null : 'Valor de ' + wordType + ' na posição ' + pos + ' do DB ' + dbIndex + ' diferente do esperado. Esperado: ' + esperado + ', encontrado: ' + recebido)
              break
            default:
              cb((recebido == esperado) ? null : 'Valor de ' + wordType + ' na posição ' + pos + ' do DB ' + dbIndex + ' diferente do esperado. Esperado: ' + esperado + ', encontrado: ' + recebido)
                  
          }
        }
      })
    })

    Given('que haja uma variável {stringInDoubleQuotes} na posição {int} do DB {int} com valor {stringInDoubleQuotes}', function (wordType, pos, dbIndex, val, cb) {
      let valor = parseValor(val, wordType)
      let buf = Buffer.alloc(words[wordType].size)
      let qtdAEscrever = (wordType == 'string') ? 26 : 1

      words[wordType].writeFunc(buf, 0, valor)
      this.s7client.WriteArea(this.s7client.S7AreaDB, dbIndex, pos, qtdAEscrever, words[wordType].word, buf, (err) => {
        cb((err) ? this.s7client.ErrorText(err) : null)
      })
    })

    When('eu definir o valor {stringInDoubleQuotes} para a variável {stringInDoubleQuotes} na posição {int} do DB {int}', function (val, wordType, pos, dbIndex, cb) {
      let valor = parseValor(val, wordType)
      let payload = {
        valor: valor
      }
      utils
        .doPost('plc', 'db' + '/' + dbIndex + '/' + wordType + '/' + pos, null, payload)
        .then(() => cb())
        .catch((err) => cb(err))
    })

    When('eu solicitar a variável {stringInDoubleQuotes} na posição {int} do DB {int}', function (wordType, pos, dbIndex, cb) {
      utils
        .doGet('plc', 'db' + '/' + dbIndex + '/' + wordType + '/' + pos)
        .then((res) => {
          this.lastHttpValResponse = JSON.parse(res.body)
          cb()
        })
        .catch((err) => cb(err))
    })

    Then('recebo uma {stringInDoubleQuotes} com valor {stringInDoubleQuotes}', function (wordType, val, cb) {
      let esperado = parseValor(val, wordType)
      let recebido = this.lastHttpValResponse

      switch (wordType){
        case 'real': 
          cb((Math.abs(recebido - esperado) < 0.0001) ? null : `Valor de '${wordType}' na posição ${pos} do DB ${dbIndex} diferente do esperado. Esperado: '${esperado}', encontrado: '${recebido}'`)
          break
        default:
          cb((recebido == esperado) ? null : `Valor recebido '${recebido}' diferente do valor esperado '${esperado}'`)
              
      }
    })
  })

})()