#encoding: utf-8
#language: pt

Funcionalidade: Manipulação de DB's no simulador de PLC via API REST
Como um usuário
Eu quero manipular os DB's do simulador de PLC
Para produzir o ambiente de teste que necessito

Cenário de Fundo:

Esquema do Cenário: Inscrição de DB no simulador de PLC
    Dado que eu insira no simulador um DB com as características:
        | índice   | tamanho   | valor_inicial   |
        | <índice> | <tamanho> | <valor_inicial> |
    Então o DB de índice <índice> existe
    E o DB de índice <índice> possui tamanho <tamanho> bytes
    E o DB de índice <índice> possui todas as posições com valor <valor_inicial>
    #Dado que eu insira no simulador um DB com as características:
    #| índice   | tamanho   | valor_inicial   |
    #| <índice> | <tamanho> | <valor_inicial> |
    #Então recebo resposta "DB <índice> não existe"

Exemplos:
    | índice | tamanho | valor_inicial |
    | 1      | 256     | 0             |
    | 1500   | 1024    | 0             |
    | 50     | 128     | 26            |

Cenário: Desinscrição de DB no simulador de PLC
    Dado que exista um DB com índice 7 no simulador de PLC
    Quando eu remover o DB de índice 7 do simulador de PLC
    Então o DB de índice 7 não existe

Cenário: Desinscrição de DB inexistente no simulador de PLC
    Dado que não exista um DB com índice 36 no simulador de PLC
    Quando eu remover o DB de índice 36 do simulador de PLC
    Então recebo resposta "DB '36' não existe"

Cenário: Desinscrição de todos os DB's no simulador de PLC
    Dado que exista um DB com índice 2 no simulador de PLC
    E que exista um DB com índice 73 no simulador de PLC
    E que exista um DB com índice 100 no simulador de PLC
    Quando eu remover todos os DB's do simulador de PLC
    Então o DB de índice 2 não existe
    E o DB de índice 73 não existe
    E o DB de índice 100 não existe

Cenário: Limpeza de DB no simulador de PLC
    Dado que exista um DB com índice 4 no simulador de PLC
    E que os seguintes valores estejam no DB de índice 4:
        | palavra | posição | valor |
        | byte    | 0       | 1     |
        | byte    | 1       | 127   |
        | word    | 2       | 1000  |
    Quando eu limpar o DB de índice 4
    Então o DB de índice 4 possui todas as posições com valor 0

Esquema do Cenário: Preenchimento de DB no simulador de PLC
    Dado que exista um DB com índice <índice> no simulador de PLC
    Quando eu preencher o DB de índice <índice> com o valor <valor_inicial>
    Então o DB de índice <índice> possui todas as posições com valor <valor_inicial>

Exemplos:
    | índice | valor_inicial |
    | 14     | 0             |
    | 15     | 55            |
    | 99     | 127           |

Cenário: Carregamento de DB no simulador de PLC
    Dado que exista um DB com índice 1500 no simulador de PLC
    Quando eu carregar o DB com índice 1500 com os valores:
        | palavra | posição | valor |
        | byte    | 10      | 12    |
        | byte    | 20      | 55    |
        | word    | 21      | 199   |
    Então os seguintes valores estão no DB de índice 1500:
        | palavra | posição | valor |
        | byte    | 10      | 12    |
        | byte    | 20      | 55    |
        | word    | 21      | 199   |
