(function () {
  'use strict'

  const { defineSupportCode } = require('cucumber')
  const utils = require('../support/utils')

  defineSupportCode(function ({
    After,
    Before
  }) {

    Before(function (scenario, cb) {
      utils
        .resolveIPByHostname('plc')
        .then((ipPLC) => {
          return new Promise((res,rej) => {
            this.s7client.ConnectTo(ipPLC, 0, 2, (err) => {
              if (err) rej (err)
              else res()
            })
          })
        })
        .then(() => { 
          return utils.doDelete('plc', 'network', 'removeAll')
        })
        .then(() => { 
          return utils.doDelete('plc', 'db', 'unregisterAll')
        })
        .then(() => { 
          this.lastHttpStatusCodeResponse = null
          this.lastHttpValResponse = null
        })
        .then(() => cb())
        .catch((err) => cb(err))
    })

    After(function (scenario, cb) {
      utils
        .doDelete('plc', 'network', 'removeAll')
        .then(() => {
          return utils.doDelete('plc', 'db', 'unregisterAll')
        })
        .then(() => {
          cb((this.s7client.Disconnect()) ? null : 'Erro na desconexão com plc')
        })
        .catch((err) => cb(err))
    })

  })

})()