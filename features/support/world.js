(function () {
  'use strict'

  const { defineSupportCode } = require('cucumber')
  const snap7 = require('node-snap7')
  // const s7client = new snap7.S7Client()

  function CustomWorld() {
    this.s7client = new snap7.S7Client()
    this.lastHttpStatusCodeResponse = null
    this.lastHttpValResponse = null
  }

  defineSupportCode(function ({
    setWorldConstructor
  }) {
    setWorldConstructor(CustomWorld)
  })
  
})()