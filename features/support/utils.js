(function() {

  const rp = require('request-promise-native')
  const dns = require('dns')

  const serviceAddresses = {
    plc: {
      hostname: process.env.PLC_HOSTNAME || 'plc',
      protocolo: 'http',
      porta: process.env.PLC_API_PORT || 8866,
      api: 'plc'
    }
  }

  const resolveIPByHostname = (hostname) => {
    return new Promise((resolve, reject) => {
      try {
        dns.resolve4(hostname, (err, addresses) => {
          if (err) resolve('127.0.0.1')
          else resolve(addresses[0])
        })
      } catch (e) {
        resolve('127.0.0.1') // se não conseguir resolver hostname, provavelmente teste é executado pelo host que não enxerga os hostnames
      }
    })
  }

  const getServiceAddress = (service) => {
    let svcAddr = serviceAddresses[service]
    return resolveIPByHostname(svcAddr.hostname)
      .then((ip) => {
        svcAddr.ip = ip
        return Promise.resolve(svcAddr)
      })
  }

  function doHttp(httpMethod, service, model, method, jsonPayload) {
    return getServiceAddress(service)
      .then((addr) => {
        return {
          uri: `${addr.protocolo}://${addr.ip}:${addr.porta}${(addr.api) ? '/'+addr.api : ''}${(model) ? '/'+model : ''}${(method) ? '/'+method : ''}`,
          method: httpMethod,
          body: jsonPayload,
          json: jsonPayload,
          simple: false,
          resolveWithFullResponse: true
        }
      })
      .then(rp)
  }

  function doPost(service, model, method, jsonPayload) {
    return doHttp('POST', service, model, method, jsonPayload)
  }

  function doGet(service, model, method, jsonPayload) {
    return doHttp('GET', service, model, method, jsonPayload)
  }

  function doDelete(service, model, method, jsonPayload) {
    return doHttp('DELETE', service, model, method, jsonPayload)
  }

  module.exports = {
    resolveIPByHostname : resolveIPByHostname,
    doPost : doPost,
    doGet : doGet,
    doDelete : doDelete
  }

})()