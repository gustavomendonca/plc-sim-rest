#encoding: utf-8
#language: pt

Funcionalidade: Manipulação de valores em DB's no simulador de PLC via API REST
    Como um usuário
    Eu quero manipular os valores nos DB's do simulador de PLC
    Para produzir o ambiente de teste que necessito

Cenário de Fundo:
    Dado que exista um DB com índice 1000 no simulador de PLC

Esquema do Cenário: Ativação de bit em DB
    Quando eu ativar o bit <bit_index> do DB 1000
    Então o valor da variável "bit" na posição <bit_index> do DB 1000 será "1"

Exemplos:
    | bit_index |
    | 0         |
    | 1         |
    | 8         |
    | 13        |
    | 255       |

Esquema do Cenário: Desativação de bit em DB
    Dado que haja uma variável "bit" na posição <bit_index> do DB 1000 com valor "1"
    Quando eu desativar o bit <bit_index> do DB 1000
    Então o valor da variável "bit" na posição <bit_index> do DB 1000 será "0"

Exemplos:
    | bit_index |
    | 0         |
    | 2         |
    | 16        |
    | 127       |
    | 111       |


Esquema do Cenário: Escrita de variável em DB
    Quando eu definir o valor "<valor>" para a variável "<palavra>" na posição <posicao> do DB 1000
    Então o valor da variável "<palavra>" na posição <posicao> do DB 1000 será "<valor>"

Exemplos:
    | palavra | posicao | valor                      |
    | bit     | 0       | 0                          |
    | bit     | 0       | 1                          |
    | bit     | 1       | 0                          |
    | bit     | 1       | 1                          |
    | bit     | 100     | 0                          |
    | bit     | 100     | 1                          |
    | bit     | 255     | 0                          |
    | bit     | 255     | 1                          |
    | byte    | 0       | 0                          |
    | byte    | 0       | 127                        |
    | byte    | 0       | 255                        |
    | byte    | 1       | 0                          |
    | byte    | 1       | 128                        |
    | byte    | 1       | 255                        |
    | byte    | 255     | 0                          |
    | byte    | 255     | 59                         |
    | byte    | 255     | 255                        |
    | word    | 0       | 0                          |
    | word    | 0       | 56                         |
    | word    | 0       | 65535                      |
    | word    | 200     | 300                        |
    | word    | 201     | 300                        |
    | word    | 254     | 0                          |
    | word    | 254     | 987                        |
    | word    | 254     | 65535                      |
    | dword   | 0       | 0                          |
    | dword   | 0       | 560                        |
    | dword   | 0       | 4294967295                 |
    | dword   | 200     | 300                        |
    | dword   | 201     | 300                        |
    | dword   | 252     | 0                          |
    | dword   | 252     | 987                        |
    | dword   | 252     | 4294967295                 |
    | real    | 0       | 0                          |
    | real    | 0       | 12.8                       |
    | real    | 0       | -123.9                     |
    | real    | 200     | 65.87                      |
    | real    | 201     | -456.874                   |
    | real    | 252     | 0                          |
    | real    | 252     | 123.56                     |
    | real    | 252     | -3215.65                   |
    | sbyte   | 0       | 0                          |
    | sbyte   | 0       | -128                       |
    | sbyte   | 0       | 127                        |
    | sbyte   | 1       | 0                          |
    | sbyte   | 1       | -128                       |
    | sbyte   | 1       | -19                        |
    | sbyte   | 1       | 127                        |
    | sbyte   | 255     | 0                          |
    | sbyte   | 255     | -1                         |
    | sbyte   | 255     | 127                        |
    | sword   | 0       | 0                          |
    | sword   | 0       | -56                        |
    | sword   | 0       | -32768                     |
    | sword   | 200     | -300                       |
    | sword   | 201     | 300                        |
    | sword   | 254     | 0                          |
    | sword   | 254     | -87                        |
    | sword   | 254     | -32768                     |
    | sdword  | 0       | 0                          |
    | sdword  | 0       | -560                       |
    | sdword  | 0       | -2147483647                |
    | sdword  | 200     | -300                       |
    | sdword  | 201     | -300                       |
    | sdword  | 252     | 0                          |
    | sdword  | 252     | -987                       |
    | sdword  | 252     | -2147483647                |
    | string  | 0       | abcdefghijklmnopqrstuvwxyz |
    | string  | 10      | abcdefghijklmnopqrstuvwxyz |
    | string  | 2       | AbCdE                      |
    | string  | 0       | 12345                      |
    | string  | 1       | a b c d e 1 2 3 4          |
    | string  | 0       | 00000000                   |


Esquema do Cenário: Leitura de variável em DB
    Dado que haja uma variável "<palavra>" na posição <posicao> do DB 1000 com valor "<valor>"
    Quando eu solicitar a variável "<palavra>" na posição <posicao> do DB 1000
    Então recebo uma "<palavra>" com valor "<valor>"

Exemplos:
    | palavra | posicao | valor                      |
    | bit     | 0       | 0                          |
    | bit     | 0       | 1                          |
    | bit     | 1       | 0                          |
    | bit     | 1       | 1                          |
    | bit     | 100     | 0                          |
    | bit     | 100     | 1                          |
    | bit     | 255     | 0                          |
    | bit     | 255     | 1                          |
    | byte    | 0       | 0                          |
    | byte    | 0       | 127                        |
    | byte    | 0       | 255                        |
    | byte    | 1       | 0                          |
    | byte    | 1       | 128                        |
    | byte    | 1       | 255                        |
    | byte    | 255     | 0                          |
    | byte    | 255     | 59                         |
    | byte    | 255     | 255                        |
    | word    | 0       | 0                          |
    | word    | 0       | 56                         |
    | word    | 0       | 65535                      |
    | word    | 200     | 300                        |
    | word    | 201     | 300                        |
    | word    | 254     | 0                          |
    | word    | 254     | 987                        |
    | word    | 254     | 65535                      |
    | dword   | 0       | 0                          |
    | dword   | 0       | 560                        |
    | dword   | 0       | 4294967295                 |
    | dword   | 200     | 300                        |
    | dword   | 201     | 300                        |
    | dword   | 252     | 0                          |
    | dword   | 252     | 987                        |
    | dword   | 252     | 4294967295                 |
    | real    | 0       | 0                          |
    | real    | 0       | 12.8                       |
    | real    | 0       | -123.9                     |
    | real    | 200     | 65.87                      |
    | real    | 201     | -456.874                   |
    | real    | 252     | 0                          |
    | real    | 252     | 123.56                     |
    | real    | 252     | -3215.65                   |
    | sbyte   | 0       | 0                          |
    | sbyte   | 0       | -128                       |
    | sbyte   | 0       | 127                        |
    | sbyte   | 1       | 0                          |
    | sbyte   | 1       | -128                       |
    | sbyte   | 1       | -19                        |
    | sbyte   | 1       | 127                        |
    | sbyte   | 255     | 0                          |
    | sbyte   | 255     | -1                         |
    | sbyte   | 255     | 127                        |
    | sword   | 0       | 0                          |
    | sword   | 0       | -56                        |
    | sword   | 0       | -32768                     |
    | sword   | 200     | -300                       |
    | sword   | 201     | 300                        |
    | sword   | 254     | 0                          |
    | sword   | 254     | -87                        |
    | sword   | 254     | -32768                     |
    | sdword  | 0       | 0                          |
    | sdword  | 0       | -560                       |
    | sdword  | 0       | -2147483647                |
    | sdword  | 200     | -300                       |
    | sdword  | 201     | -300                       |
    | sdword  | 252     | 0                          |
    | sdword  | 252     | -987                       |
    | sdword  | 252     | -2147483647                |
    | string  | 0       | abcdefghijklmnopqrstuvwxyz |
    | string  | 10      | abcdefghijklmnopqrstuvwxyz |
    | string  | 208     | AbCdE                      |
    | string  | 78      | 12345                      |
    | string  | 26      | a b c d e 1 2 3 4          |
    | string  | 0       | 00000000                   |

#Para garantir que a escrita subsequente de um outro valor no memso db não impacta na escrita anterior
#Testa ida e volta pela própria API
Esquema do Cenário: Escrita/Leitura de múltiplas variáveis em um DB de <palavra>
    Quando eu definir o valor "<valor1>" para a variável "<palavra>" na posição <posicao1> do DB 1000
    E eu definir o valor "<valor2>" para a variável "<palavra>" na posição <posicao2> do DB 1000
    Quando eu solicitar a variável "<palavra>" na posição <posicao1> do DB 1000
    Então recebo uma "<palavra>" com valor "<valor1>"
    Quando eu solicitar a variável "<palavra>" na posição <posicao2> do DB 1000
    Então recebo uma "<palavra>" com valor "<valor2>"

Exemplos:
    | palavra | posicao1 | valor1       | posicao2 | valor2       |
    | bit     | 3        | 1            | 6        | 1            | 
    | byte    | 9        | 241          | 99       | 31           | 
    | word    | 24       | 10000        | 11       | 9876         | 
    | dword   | 250      | 32165649     | 123      | 4294967295   | 
    | real    | 219      | 11.11        | 150      | -456.874     | 
    | sbyte   | 55       | -1           | 33       | 99           |
    | sword   | 87       | -32768       | 12       | 6168         |
    | sdword  | 63       | -2147483647  | 222      | 651984621    | 
    | string  | 52       | abcd_1234_=) | 26       | wxyz_9876_=( | 