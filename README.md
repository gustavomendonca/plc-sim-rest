# Simulador de PLC Siemens

O simulador de PLC Siemens insere na rede um PLC virtual controlado por uma API REST. Para todos os efeitos, este PLC é visto como um PLC real, inclusive pelo software STEP7.

## Requisitos 

- Docker 
- Docker-compose (opcional)

## Como usar

### pelo docker

Para compilar o container:

```sh
$ docker build . -t plc
```
Para executar:
```sh
$ docker run -it --rm --cap-add NET_BIND_SERVICE -e "PLC_HOSTNAME=plc" -e "PLC_API_PORT=8866" -p 0.0.0.0:8866:8866 -p 0.0.0.0:102:102 -p 0.0.0.0:5858:5858 -v $PWD:/plc-sim-rest -w /plc-sim-rest plc sh -c "npm install && nodemon --inspect=5858"
```
### pelo docker-compose
Mais simples, porém exige instalação do **docker-compose**. 
```
docker-compose up 
```

#### Cadastrar DB
Ao executar o simulador de plc, este não possui nenhum DB cadastrado. Para cadastrar o DB 100 com 80 bytes de capacidade e valor inicial 0:
```sh
curl -X POST \
  http://localhost:8866/plc/db/100/register \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 7cf87e3c-fad8-0229-d0bc-fd0d6f12c01f' \
  -d '
 {
  "size": 80,
  "fillValue": 0
}'
```

## Funcionamento

O projeto é baseado em um container do Docker que expõe duas portas no host:

- 102 - Porta de comunicação do protocolo S7 utilizada pelos PLC Siemens.
- 8866 - API de controle.

## Palavras

O simulador de PLC trabalha com os seguintes tipos de dados:

| Palavra        | Descrição                         |
|----------------|-----------------------------------|
| bit            | 1 bit                             |
| byte           | unsigned integer (8 bits)         |
| word           | unsigned integer (16 bits)        |
| dword          | unsigned integer (32 bits)        |
| real           | float point (32 bits)             |
| string         | array de 26 x 8 bits (padrão TCS) |
| sbyte          | signed integer (8 bits)           |
| sword          | signed integer (16 bits)          |
| sdword         | signed integer (32 bits)          |

## API

A API REST aceita os verbos **GET**, **POST** e **DELETE** e utiliza objetos JSON.

### root

#### GET / 

##### retorno
`{ "result":"ok" }`

### Manipulação de DB's

#### POST /db/[db_index]/register

Registra DB [db_index] no simulador de PLC.

##### Parâmetros

- **size**: tamanho do DB em bytes.
- **fillChar**(opcional): valor inicial de todos os bytes.

#### DELETE /db/[db_index]/unregister

Retira DB [db_index] do simulador de PLC.

#### DELETE /db/unregisterAll

Retira todos os DBs do simulador de PLC.

#### POST /db/[db_index]/fill

Preenche todos os bytes do DB [db_index] com o valor especificado.

##### Parâmetros

- **fillChar**: valor a ser preenchido em todos os bytes.

#### POST /db/[db_index]/clear

Limpa o DB[db_index] inteiro (define todos os bytes como 0x00).

#### POST /db/[db_index]/load

Carrega DB [db_index] com valores passados.

##### Parâmetros

- **valores**[array]: contendo objetos do tipo:
  - **palavra**: string. Ver [Palavras](#palavras).
  - **posição**: índice do DB onde palavra inicia.
  - **valor**: valor a ser carregado.

### Leitura/Escrita de variáveis

#### GET /db/[db_index]/[palavra]/[posicao]

Retorna valor da **[[palavra]](#palavras)** na posicao **[posicao]** do DB **[db_index]**

#### POST /db/[db_index]/[palavra]/[posicao]

Define valor da **[[palavra]](#palavras)** na posicao **[posicao]** do DB **[db_index]**

##### Parâmetros

- **valor**: valor a ser definido