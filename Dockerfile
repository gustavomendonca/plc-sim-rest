FROM node:carbon

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        build-essential \
        gcc \
        iproute2
RUN rm -rf /var/lib/apt/lists/*

RUN npm install -g nodemon