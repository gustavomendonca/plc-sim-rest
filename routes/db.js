(function () {
  'use strict';

  const plc = require('../plc')

  exports.register = (req, res) => {
    let index = parseInt(req.params.dbIndex, 10)
    let size = req.body.size
    if (index > 0 && size > 0) {
      let fillValue = req.body.fillValue || 0x00
      let db = Buffer.alloc(size, fillValue)
      plc
        .registerDB(index, db)
        .then(() => res.status(200).json(`DB '${index}' inscrito`))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com corpo inválido: ' + JSON.stringify(req.body))
    }
  }

  exports.unregister = (req, res) => {
    let index = parseInt(req.params.dbIndex, 10)
    if (index > 0) {
      plc
        .unregisterDB(index)
        .then(() => res.status(200).json(`DB '${index}' desinscrito`))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com corpo inválido: ' + JSON.stringify(req.body))
    }
  }

  exports.unregisterAll = (req, res) => {
    plc
      .unregisterAllDBs()
      .then(() => res.status(200).json('Todos os DB\'s desinscritos'))
      .catch((err) => res.status(500).json(err.message))
  }

  exports.fill = (req, res) => {
    let index = parseInt(req.params.dbIndex, 10)
    let fillChar = req.body.fillChar
    if (index > 0 && fillChar.length === 1) {
      plc
        .fillDB(index, fillChar)
        .then(() => res.status(200).json(`DB '${index}' preenchido com '${fillChar}'`))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com corpo inválido: ' + JSON.stringify(req.body))
    }
  }

  exports.clear = (req, res) => {
    let index = parseInt(req.params.dbIndex, 10)
    if (index > 0) {
      plc
        .fillDB(index, 0x00)
        .then(() => res.status(200).json(`DB '${index}' limpo`))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com corpo inválido: ' + JSON.stringify(req.body))
    }
  }

  exports.load = (req, res) => {
    let index = parseInt(req.params.dbIndex, 10)
    let valores = req.body.valores
    if (index > 0 && valores) {
      plc
        .loadDB(index, valores)
        .then(() => res.status(200).json(`DB '${index}' carregado`))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com corpo inválido: ' + JSON.stringify(req.body))
    }
  }

  exports.get = (req, res) => {
    let index = parseInt(req.params.dbIndex, 10)
    if (index > 0) {
      plc
        .getDB(index)
        .then((db) => res.status(200).json(db))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com corpo inválido: ' + JSON.stringify(req.body))
    }
  }

})()