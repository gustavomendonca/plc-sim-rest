(function () {
  'use strict';

  const plc = require('../plc')

  exports.insert = (req, res) => {
    let network = req.body
    if (network){
      plc
        .insertNetwork(network)
        .then(() => res.status(200).json(`Network '${network.nome}' inserida`))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com corpo inválido: ' + JSON.stringify(req.body))
    }
  }

  exports.remove = (req, res) => {
    let name = req.body.nome
    if (name){
      plc
        .removeNetwork(name)
        .then(() => res.status(200).json(`Network '${name}' removida`))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com corpo inválido: ' + JSON.stringify(req.body))
    }
  }

  exports.removeAll = (req, res) => {
    plc
      .removeAllNetworks()
      .then(() => res.status(200).json('Networks removidas'))
      .catch((err) => res.status(500).json(err.message))
  }

})()