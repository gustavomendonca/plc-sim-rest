(function () {
  'use strict';

  const plc = require('../plc')

  exports.get = (req, res) => {
    let dbIndex = parseInt(req.params.dbIndex, 10)
    let pos = parseInt(req.params.pos, 10)
    let path = req.path.slice(0, req.path.lastIndexOf('/'))
    let wordType = path.slice(path.lastIndexOf('/') + 1, path.len)
    if (dbIndex > 0 && pos >= 0) {
      plc
        .getVarDB(dbIndex, wordType, pos)
        .then((val) => res.status(200).json(val))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com parâmetros inválidos: dbIndex: ' + dbIndex + ' - pos: ' + pos)
    }
  }

  exports.set = (req, res) => {
    let dbIndex = parseInt(req.params.dbIndex, 10)
    let pos = parseInt(req.params.pos, 10)
    let path = req.path.slice(0, req.path.lastIndexOf('/'))
    let wordType = path.slice(path.lastIndexOf('/') + 1, path.len)
    if (dbIndex > 0 && pos >= 0) {
      let val = req.body.valor
      plc
        .setVarDB(dbIndex, wordType, pos, val)
        .then((val) => res.status(200).json(val))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com parâmetros inválidos: dbIndex: ' + dbIndex + ' - pos: ' + pos)
    }
  }

})()