(function () {
    'use strict';
  
    const plc = require('../plc')
  
    exports.carrega = (req, res) => {
      const mapaNomes = {
        STRING: 'string',
        BYTE: 'byte',
        REAL: 'real',
        INT: 'word',
        GERAL: 'string'
      }
      let dbs = req.body

      let promises = dbs
      .map((db) => {
        db.valores = db.valores.map((val) => {
          val.palavra = mapaNomes[db.palavra]
          return val
        })
        return db
      })
      .map((db) => plc.loadDB(db.index, db.valores))
      
      Promise.all(promises)
        .then(() => res.status(200).json('Rastreamento carregado'))
        .catch(err => {
          console.log('[ERRO]', err)
          res.status(500).json(err)
        })
     }
  
  })()