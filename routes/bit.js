(function () {
  'use strict';

  const plc = require('../plc')

  exports.get = (req, res) => {
    let dbIndex = parseInt(req.params.dbIndex, 10)
    let bitIndex = parseInt(req.params.bitIndex, 10)
    if (dbIndex > 0 && bitIndex >= 0) {
      plc
        .getVarDB(dbIndex, 'bit', bitIndex)
        .then((val) => res.status(200).json(val))
        .catch((err) => res.status(500).json(err.message)) 
    } else {
      res.status(400).json('Http POST com parâmetros inválidos: dbIndex: ' +dbIndex + ' - bitIndex: ' + bitIndex)
    }
  }

  exports.set = (req, res) => {
    let dbIndex = parseInt(req.params.dbIndex, 10)
    let bitIndex = parseInt(req.params.bitIndex, 10)
    if (dbIndex > 0 && bitIndex >= 0) {
      let val = parseInt(req.body.valor)
      plc
        .setVarDB(dbIndex, 'bit', bitIndex, val)
        .then((val) => res.status(200).json(val))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com parâmetros inválidos: dbIndex: ' + dbIndex + ' - bitIndex: ' + bitIndex)
    }
  }

  exports.set0 = (req, res) => {
    let dbIndex = parseInt(req.params.dbIndex, 10)
    let bitIndex = parseInt(req.params.bitIndex, 10)
    if (dbIndex > 0 && bitIndex >= 0) {
      plc
        .setVarDB(dbIndex, 'bit', bitIndex, 0x00)
        .then((res) => res.status(200).json(res))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com parâmetros inválidos: dbIndex: ' + dbIndex + ' - bitIndex: ' + bitIndex)
    }
  }

  exports.set1 = (req, res) => {
    let dbIndex = parseInt(req.params.dbIndex, 10)
    let bitIndex = parseInt(req.params.bitIndex, 10)
    if (dbIndex > 0 && bitIndex >= 0) {
      plc
        .setVarDB(dbIndex, 'bit', bitIndex, 0x01)
        .then((res) => res.status(200).json(res))
        .catch((err) => res.status(500).json(err.message))
    } else {
      res.status(400).json('Http POST com parâmetros inválidos: dbIndex: ' + dbIndex + ' - bitIndex: ' + bitIndex)
    }
  }

})()